Amazing Grace Wordpress Themes
http://www.prelovac.com/vladimir

This Theme is under GPL License http://www.opensource.org/licenses/gpl-license.php

INSTALL: 
1. Upload the contents of this archive into your wp-content/themes/ directory on WordPress server.
2. Go to WordPress administration into theme selection
4. Activate Amazing Grace

Current List of Features:
    * Modern styled, SEO Optimized, Clean, 3 columns, Photo-friendly, Widget ready    
    * Valid XHTML/CSS document
    * WordPress 3.0+ support (threaded comments, menu navigation system)
    * Optimized for speed and loading time
    * Out of box full SEO optimization including titles and page headings    
    * Out of box Social networking support
    * Customized 404 page
    * Adsense ready
    * Plus much more...

You may alternatively want to use Theme Test Drive http://www.prelovac.com/vladimir/wordpress-plugins/theme-test-drive 
to test the theme on your blog before running it live.

Questions, bugs and others can be posted on http://www.prelovac/com/vladimir/forum

Special credits to Jos� Filipe for translation.

Vladimir Prelovac
http://www.prelovac.com/vladimir