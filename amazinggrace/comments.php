<?php 
if ( post_password_required() ) : ?>
<p class="comments-nopassword"><?php _e( "This post and it's comments are password protected. Enter the password into the form above to view any comments.", 'amazinggrace' ); ?></p>
<?php
// Stop the rest of comments.php from being processed,
return;
endif;
?>

<?php if(have_comments()) : ?>
<h2 id="comments" class="total-comments">
<?php 
$args = array(
	'zero' => __('No comments', 'amazinggrace'),
	'one' => __('1 Comment', 'amazingrace'),
	'more' => __('% Comments', 'amazinggrace')
);
comments_number($args ); ?> <?php _e('on', 'amazinggrace');?> <?php the_title(); ?></h2>
<?php endif;?>


<?php if(have_comments()) : ?>

<?php paginate_comments_links('type=list'); ?>

<ol id="commentlist">
<?php wp_list_comments(); ?>
</ol>

<?php paginate_comments_links('type=list'); ?>

<?php endif;

comment_form( amazinggrace_comment_form_args($user_identity, $post->ID, $req) ); 