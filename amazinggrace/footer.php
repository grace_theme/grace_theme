<?php get_sidebar(); ?>
</div> 

<div id="footer">

<div id="credits">
	
	<div id="ftnav">
		<span class="rss"><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Subscribe to RSS', 'amazinggrace'); ?>"><abbr title="<?php _e('Subscribe to RSS', 'amazinggrace'); ?>">RSS</abbr></a></span>
	</div>
	     <?php grace_footer(); ?>
	</div>		
</div>
<?php wp_footer(); ?>
</body>
</html>
