<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php wp_title(' | ', true, ''); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<style type="text/css">
  #portrait-bg { background:url(<?php bloginfo('template_directory'); ?>/images/ig-portrait<?php echo (rand()%9); ?>.jpg); }
</style>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if(is_singular()) wp_enqueue_script( 'comment-reply' );?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="wrap">
	<div id="menu">
		
			<?php wp_nav_menu( array('menu' => 'Top Navigation', 'fallback_cb' => 'grace_top_default_menu' )); ?>			
	
	</div>
	
	<div id="header">
		<span class="btitle"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></span>
		<p class="description">
			<a href="<?php 
			if (current_user_can('edit_posts')) 
				echo get_option('home').'/wp-admin/">'; 
			else 
				echo get_option('home').'/">'; 
			bloginfo('description'); ?> 
			</a>
		</p>
	</div>
	
	<div id="rss-big">
		<a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Subscribe to this site with RSS', 'amazinggrace'); ?>"></a>
	</div>
	
	<div id="portrait-bg"></div>
	<div id="catmenu">
	
			<?php wp_nav_menu( array('menu' => 'Bottom Navigation', 'fallback_cb' => 'grace_bottom_default_menu' ));  ?>
		
	</div>
