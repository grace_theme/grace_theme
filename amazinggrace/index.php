<?php get_header(); 
$loopcounter=0;
?>
<div id="content">
	<?php $current_tag = single_tag_title("", false); if ($current_tag) echo '<div class="tagarchive"><h1>'.ucwords($current_tag).'</h1></div>'?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); $loopcounter++; ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry entry-<?php echo $postCount ;?>">
			
				<div class="entrytitle_wrap">
					<?php if (!is_page()) : ?>
						<div class="entrydate">
							<div class="dateMonth">
								<?php the_time('M');?>
							</div>
							<div class="dateDay">
								<?php the_time('j'); ?>
							</div>
						</div>
					<?php endif; ?>
				
					<div class="entrytitle">
					<?php if ($loopcounter==1):?>  
						<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Link to %s', 'amazinggrace'), the_title_attribute()); ?>"><?php the_title(); ?></a></h1> 
					<?php else : ?>
						<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Link to %s', 'amazinggrace'), the_title_attribute()); ?>"><?php the_title(); ?></a></h2> 
					<?php endif; ?>
					</div>
			
<<<<<<< HEAD
					<?php if (!is_singular()): ?>
						<div class="endate"><?php the_author(); _e(' on ', 'amazinggrace'); the_time(__('F jS, Y', 'amazinggrace')); ?></div>
					<?php endif; ?>	
=======
          <div class="endate">Posted by <?php the_author(); ?> on <?php the_time('F jS, Y'); ?></div>
>>>>>>> 36bcbc89aafb5bfbd75fb43e8d6bc67db62a97b7
				</div>
			
			
				<div class="entrybody">	
					<?php if (is_archive() || is_search()) : ?>	
						<?php the_excerpt(); ?><p><?php printf('<a href="%s">', get_permalink()); _e('Continue reading about ', 'amazinggrace'); the_title(); ?></a></p>
					<?php else : ?>
						<?php the_content(__('Read the rest of this entry &raquo;', 'amazinggrace'));   ?>
						<?php wp_link_pages(); ?>
						<?php the_tags('<p>'.__('Tags: ', 'amazinggrace'), ', ', '</p>'); ?>
					<?php endif; ?>			
				</div>
			
				<div class="entrymeta">	
					<div class="postinfo"> 
				
						<?php if ($loopcounter==1) social_bookmarks(); ?>	
						<?php if (is_single()): ?>
						 <span class="postedby"><?php printf(__('Posted by %s', 'amazinggrace'), get_the_author()); ?></span>
						<?php endif; ?>
						
						<?php if (!is_page()): ?>
							<span class="filedto"><?php the_category(', ') ?> </span>
						<?php endif; ?>
						
						<?php if (!is_singular()): ?>
							<span class="commentslink"><?php comments_popup_link(__('No comments &#187;', 'amazinggrace'), __('1 Comment &#187;', 'amazinggrace'), _n('% Comment &#187;', '% Comments &#187;', get_comments_number(), 'amazinggrace'));?></span>  					
						<?php else: ?>
							<span class="rss"><?php _e('Subscribe to ', 'amazinggrace') ?><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Subscribe to RSS feed', 'amazinggrace'); ?>" ><abbr title="<?php _e('Subscribe to RSS Feed', 'amazinggrace'); ?>">RSS</abbr></a> feed</span>
						<?php endif; ?>
				
                                                <?php the_shortlink( 'Shortlink', '', ' | ', ''); ?>
						<?php edit_post_link(__('Edit', 'amazinggrace'), ' | ', ''); ?>
				
					</div>	
				</div>
			
			                    
				<?php if ($loopcounter == 1 && !is_singular()) { get_template_part('ad_middle.php'); } ?>                 
			
			</div>	
			
			<?php if (is_singular()): ?>
				<div class="commentsblock">
					<?php comments_template(); ?>
				</div>
			<?php endif; ?>
		
	</div>
	
	<?php endwhile; ?>
	
	<?php if (!is_singular()): ?>         
		<div id="nav-global" class="navigation">
			<div class="nav-previous">
			<?php 
				next_posts_link(__('&laquo; Previous entries', 'amazinggrace'));
				echo '&nbsp;';
				previous_posts_link(__('Next entries &raquo;', 'amazinggrace'));
			?>
			</div>
		</div>
		
	<?php endif; ?>
		
	<?php else : ?>
	
		<h2><?php _e('Not Found', 'amazinggrace') ?></h2>
		<div class="entrybody"><?php _e("Sorry, but you are looking for something that isn't here.", "amazinggrace"); ?></div>
	<?php endif; ?>
	
</div>

<?php get_footer(); ?>
